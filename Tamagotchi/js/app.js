/**
 * Created by tfaas on 8/31/17.
 */


    //class is _not fully supported_
class Tamagotchi {

    //constructs this object - helps to get it set up
    constructor(name, type, energy, selector) {
        this.name = name;
        this.type = type;
        this.energy = energy;
        this.element = document.querySelector(selector);

        //start the tamagochi simulation
        setInterval(this.update.bind(this), 1000);
    }

    //exposes some debug data...
    report() {
        console.log(this.name + " is a " + this.type + " and has e" + this.energy);
    }

    //add energy to tamagotchi
    eat() {
        //add one energy
        this.energy += 10;
    }

    //common video game method
    //runs the tamagotchi 'sim'
    update() {
        this.energy --;
        this.element.innerHTML = this.name + " has " + this.energy + " energy";
    }
}

//make and instance of a tamagotchi
var myTamagotchi =  new Tamagotchi("Druid", "heart", 24, "#dvInfo");
myTamagotchi.report(); //have the tamagotchi expose some debug data



